# Andrew Zurita

Hi!, I'm an Electronics Engineer who wishes to have a career in the beautiful field of Tech

My focus area is in Full Stack Web Development and have been using the MERN Stack, I am now in training as a Mobile App developer with the use of the Flutter Framework.

As an engineer, I love to make things that will benefit a lot of people and will make their life a lot easier

# Contact Information

LinkedIn: [https://www.linkedin.com/in/catzurita/](https://www.linkedin.com/in/catzurita/)

Website Portfolio: [https://zurita-flutter-portfolio.herokuapp.com/#/](https://zurita-flutter-portfolio.herokuapp.com/#/)

# Work Experience

## Flutter Programmer Trainee

FFUF Manila

- I am currently a trainee as a Mobile App developer, the main language and framework we are using are Dart and Flutter

## Full Stack Development Trainee

Zuitt Coding Bootcamp, March to April

- Attended a boot camp to be a Full Stack Developer, we studied about Html, Javascript, CSS and the MERN stack

## Site Engineer

Taikisha Philippines Inc. February 2019 - August 2020

- Site Engineer for the Electronics and Electrical Systems

# Skills

## Technical Skills

- Programming
- Software Engineering
- Mobile App Development

## Soft Skills

- Good Rapport
- Team Player
- Problem Solver
- The interest to learn more
- Quick learner

# Education

## BS Electronics and Communication Engineer

Adamson University, 2012 -2017